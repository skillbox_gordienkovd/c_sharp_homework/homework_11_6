﻿using System.IO;
using System.Text.Json;

namespace homework_11_6
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var department = new Department(3, 3);
            File.WriteAllText(@"d:\departments.json", JsonSerializer.Serialize(department));
        }
    }
}