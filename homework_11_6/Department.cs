using System;
using System.Collections.Generic;

namespace homework_11_6
{
    public class Department
    {
        public Department()
        {
        }

        public Department(int numOfChildren, int numOfLayers) : this(numOfChildren, numOfLayers, 0, new int[1])
        {
        }

        private Department(int numOfChildren, int numOfLayers, int layerIndex, int[] nodeIndex)
        {
            Layer = layerIndex;
            NodeId = nodeIndex[0]++;
            Name = $"Департамент_{layerIndex}{nodeIndex[0]}";
            Employees = CreateEmployees(numOfLayers + layerIndex);

            if (numOfLayers <= 0 || numOfChildren <= 0) return;
            Departments = new List<Department>();
            for (var i = 0; i < numOfChildren; i++)
                Departments.Add(new Department(numOfChildren, numOfLayers - 1, Layer + 1, nodeIndex));
        }

        public string Name { get; set; }
        public int Layer { get; set; }
        public int NodeId { get; set; }
        public List<Employee> Employees { get; set; }
        public List<Department> Departments { get; set; }

        private List<Employee> CreateEmployees(int num)
        {
            const int departmentSalary = 31 * 150 + 300;
            var directorSalary = (int) ((departmentSalary + Math.Pow(num, num - Layer) * departmentSalary) * 0.15);
            return new List<Employee>(3)
            {
                new Director {FirstName = "Иван", LastName = "Иванов", Salary = directorSalary},
                new Worker {FirstName = "Петр", LastName = "Петров", Salary = 31 * 150},
                new Intern {FirstName = "Петр", LastName = "Петров", Salary = 300}
            };
        }
    }
}